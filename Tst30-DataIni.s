; $Id: Tst30-DataIni.s,v 1.2 2006-04-20 14:51:28-04 jjmcd Exp jjmcd $
;		Clear out the data buffer Tst30
       .equ __30F2010, 1
        .include "p30f2010.inc"

		.global		data_init
		.extern		pointer,y_input

;..............................................................................
;Subroutine: Clear data area
;..............................................................................
data_init:
		MOV			#y_input,W4		; Point to first word
		REPEAT		#0x7f
		CLR			[W4++]			; Clear word, autoincrement

		MOV			#y_input,W4		; to first word
		MOV			W4,pointer		; Setup pointer for later
		RETURN

.end
