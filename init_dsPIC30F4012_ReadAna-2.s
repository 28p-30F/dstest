
; Initialization Code for dsPIC30F4012, Family: motor control, Package: 28-Pin SPDIP 28pins

.include "p30F4012.inc"



; Filter Controls used to Generate Code:

; POR Match Filter OFF

; Provisioned Feature Filter ON

; Masks are Ignored and uses UnMasked Register Writes

.GLOBAL _VisualInitialization

; Feature=fuses - fuses (DCR) configuration

; B15:14=FSCKM1:0 B9:8=FOS1:0 B3:0=FPR3:0
	config __FOSC, 0xC307

; B15=FWDTEN B5:4=FWPSA1:0 B3:0=FWPSB3:0
	config __FWDT, 0x803F

; B15=MCLREN B10=PWMPIN B9=HPOL B8=LPOL

; B7=BOREN B5:4=BORV1:0 B3:0=FPWRT3:0
	config __FBORPOR, 0x87B3

.text

_VisualInitialization:

; Feature=Interrupts - Disable Interrupts during configuration

; clear int flags:

; B15=CN B14=BCL B13=I2C B12=NVM B11=AD B10=U1TX B9=U1RX B8=SPI1

; B7=T3 B6=T2 B5=OC2 B4=IC2 B3=T1 B2=OC1 B1=IC1 B0=INT0
	CLR IFS0

; B15:12=IC6:3 B11=C1 B10=SPI2 B9=U2TX B8=U2RX

; B7=INT2 B6=T5 B5=T4 B4=OC4 B3=OC3 B2=IC8 B1=IC7 B0=INT1
	CLR IFS1

; B12=FLTB B11=FLTA B10=LVD B9=DCI B8=QEI

; B7=PWM B6=C2 B5=INT4 B4=INT3 B3:0=OC8:5
	CLR IFS2
	CLR IEC0
	CLR IEC1
	CLR IEC2

; Feature=Reset - Reset configuration

; B15=TRAPR B14=IOPWR B13=BGST B12=LVDEN B11:8=LVDL3:0

; B7=EXTR B6=SWR B5=SWDTEN B4=WDTO B3=SLEEP B2=IDLE B1=BOR B0=POR
	MOV #0x0003, W0
	MOV W0, RCON

; Feature=NVM - NVM configuration - not implemented

; Feature=Oscillator - Oscillator configuration

; method to override OSCCON write protect

; B13:12=COSC1:0 B9:8=NOSC1:0
	MOV.B #0x03, W0

	MOV.B #0x78, W1

	MOV.B #0x9A, W2

	MOV.W #OSCCON, W3

	MOV.B W1, [W3+1]

	MOV.B W2, [W3+1]

	MOV.B W0, [W3+1]

; B7:6=POST1:0 B5=LOCK B3=CF B1=LPOSCEN B0=OSWEN
	CLR.B W0

	MOV.B #0x46, W1

	MOV.B #0x57, W2

	MOV.B W1, [W3+0]

	MOV.B W2, [W3+0]

	MOV.B W0, [W3+0]

; Feature=A2D - A2D configuration
                    ; force all A2D ports to digital IO at first
	MOV #0xFFFF, W0
	MOV W0, ADPCFG

; Feature=IOPortE - IO Ports configuration

; B15:0=D15:0
	MOV #0x0006, W0                        ; enable
	MOV W0, PORTE
	MOV #0xFFF0, W0                        ; direction in=1
	MOV W0, TRISE

; Feature=Timer1 - Timers configuration
	CLR T1CON                              ; stop timer

; Feature=Timer1 - Timers configuration
	CLR TMR1                               ; timer register
	MOV #0x0020, W0                        ; period register
	MOV W0, PR1

; Feature=UART1 - UART 1 configuration
	MOV #0x00BF, W0                        ; UART1 baud rate generator
	MOV W0, U1BRG

; B15=UTXISEL B11=UTXBRK B10=UTXEN B9=UTXBF B8=TRMT

; B7:6=URXISEL1:0 B5=ADDEN B4=RIDLE B3=PERR B2=FERR B1=OERR B0=URXDA
	MOV #0x8000, W0                        ; enabling UART flushes buffers
	MOV W0, U1MODE
	MOV #0x0510, W0
	MOV W0, U1STA

; B15=UARTEN B13=USIDL B10=ALTIO

; B7=WAKE B6=LPBACK B5=ABAUD B2:1=PDSEL1:0 B0=STSEL
	MOV #0x8000, W0
	MOV W0, U1MODE

; Feature=A2D - A2D configuration
                    ; Turn off A2D before setting registers
	CLR ADCON1

; B15:0=CSSL15:0
	MOV #0x0021, W0
	MOV W0, ADCSSL

; B15:14=CH123NB1:0 B13=CH123SB B12=CH0NB B11:8=CH0SB3:0

; B7:6=CH123NA1:0 B5=CH123SA B4=CH0NA B3:0=CH0SA3:0
	MOV #0x0005, W0
	MOV W0, ADCHS

; B15:0=PCFG15:0
	MOV #0x001F, W0
	MOV W0, ADPCFG

; B12:8=SAMC4:0 B7=ADRC B5:0=ADCS5:0
	CLR ADCON3

; B15:13=VCFG2:0 B12=OFFCAL B10=CSCNA B9:8=CHPS1:0

; B7=BUFS B5:2=SMPI B1=BUFM B0=ALTS
	CLR ADCON2

; B15=ADON B13=ADSIDL B12=ADSTBY B9:8=FORM

; B7:5=SSRC B3=SIMSAM B2=ASAM B1=SAMP B0=CONV
	MOV #0x83E4, W0
	MOV W0, ADCON1

; Feature=required - Interrupt flags cleared and interrupt configuration

; interrupt priorities IP

; B14:12=T1 B10:8=OC1 B6:4=IC1 B2:0=INTO
	MOV #0x4444, W0
	MOV W0, IPC0

; B14:12=T3 B10:8=T2 B6:4=OC2 B2:0=IC2
	MOV #0x4444, W0
	MOV W0, IPC1

; B14:12=AD B10:8=U1TX B6:4=U1RX B2:0=SPI1
	MOV #0x4444, W0
	MOV W0, IPC2

; B14:12=CN B10:8= BCLB6:4=I2C B2:0=NVM
	MOV #0x4444, W0
	MOV W0, IPC3

; B14:12=OC3 B10:8=IC8 B6:4=IC7 B2:0=INT1
	MOV #0x4444, W0
	MOV W0, IPC4

; B14:12=INT2 B10:8=T5 B6:4=T4 B2:0=OC4
	MOV #0x4444, W0
	MOV W0, IPC5

; B14:12=C1 B10:8=SPI2 B6:4=U2TX B2:0=U2RX
	MOV #0x4444, W0
	MOV W0, IPC6

; B14:12=PWM B10:8=C2 B6:4=INT4 B2:0=INT3
	MOV #0x4444, W0
	MOV W0, IPC9

; B14:12=FLTA B10:8=LVD B6:4=DCI B2:0=QEI
	MOV #0x4044, W0
	MOV W0, IPC10

; external interrupt enables

; B15=NSTDIS B10=OVATE B9=OVBTE B8=COVTE

; B4=MATHERR B3=ADDRERR B2=STKERR B1=OSCFAIL
	CLR INTCON1

; B15=ALTIVT B4:0=INTnEP4:0
	CLR INTCON2

; Feature=Timer1 - Start timers

; Timers1: B15=TON B13=TSIDL B6=TGATE B5:4=TCKPS1:0 B2=TSYNC B1=TCS
	MOV #0x8034, W0
	MOV W0, T1CON

; Feature=CPU - CPU register configuration
	CLR SR
	CLR SR
	CLR W0
	CLR W1
	CLR W2

; Feature=Interrupts - enable interrupts

; feature interrupt enables IE

; B15=CN B14=BCL B13=I2C B12=NVM B11=AD B10=U1TX B9=U1RX B8=SPI1

; B7=T3 B6=T2 B5=OC2 B4=IC2 B3=T1 B2=OC1 B1=IC1 B0=INT0
	MOV #0x0008, W0
	MOV W0, IEC0

; B15:12=IC6:3 B11=C1 B10=SPI2 B9=U2TX B8=U2RX

; B7=INT2 B6=T5 B5=T4 B4=OC4 B3=OC3 B2=IC8 B1=IC7 B0=INT1
	CLR IEC1

; B12=FLTB B11=FLTA B10=LVD B9=DCI B8=QEI

; B7=PWM B6=C2 B5=INT4 B4=INT3 B3:0=OC8:5
	CLR IEC2

	return
