/*! \file myIni.c

 \brief Port initialization for ReadAna

  This function sets the configuration bits and initializes the
  hardware for ReadAna.c.
*/
// Basic 30F4012 definitions
#include <p30F4012.h>
// A/D converter bit definitions
#include "peripheral_30F_24H_33F/adc10.h"


//Macros for Configuration Fuse Registers:
//Invoke macros to set up  device configuration fuse registers.
//The fuses will select the oscillator source, power-up timers, watch-dog
//timers, BOR characteristics etc. The macros are defined within the device
//header files. The configuration fuse registers reside in Flash memory.
//! Oscillator configuration
_FOSC(CSW_FSCM_OFF & XT_PLL16);  //Run this project using an external crystal
                                //routed via the PLL in 16x multiplier mode
                                //For the 7.3728 MHz crystal we will derive a
                                //throughput of 7.3728e+6*16/4 = 29.4 MIPS(Fcy)
                                //,~33.9 nanoseconds instruction cycle time(Tcy).
//! Watchdog configuration
_FWDT(WDT_OFF);                 //Turn off the Watch-Dog Timer.
//! Power up timer and MCLR configuration
_FBORPOR(MCLR_EN & PWRT_64);    //Enable MCLR reset pin and turn on the
                                //power-up timers.
//! Code protection configuration
_FGS(CODE_PROT_OFF);            //Disable Code Protection



// Constants for baud rate setup
//! On-board Crystal frequency
#define XTFREQ          7372800
//! On-chip PLL setting
#define PLLMODE         16
//! Instruction Cycle Frequency
#define FCY             XTFREQ*PLLMODE/4
//! Required baud rate
#define BAUDRATE         9600       
//!  Value for the baud rate generator
#define BRGVAL          ((FCY/BAUDRATE)/16)-1

//! myIni - Initialize hardware for ReadAna
/*! myIni() sets up the A/D converter, PORT E, Timer 1
    and the UART.

Pseudocode:
\code
Set pin AN5 to analog
Use system clock for analog 32 Tcy
Scan only AN5 and use fractional format with autosample

Set LATE to 0x06
Set PORTE all outputs

Set Timer1 to interrupt 4 times per second

Set the UART to 9600 baud
\endcode
*/
void myIni( void )
{
	// Setup A/D Converter
	ADPCFG = ENABLE_AN5_ANA;		// AN5 to analog
	ADCON2 = ADC_VREF_AVDD_AVSS & 	// Vdd/Vss references
			ADC_SCAN_OFF & 			// No scanning
			ADC_CONVERT_CH0 & 		// Channel zero only
			ADC_SAMPLES_PER_INT_5 &	// 5 samples per interrupt
			ADC_ALT_BUF_OFF & 		// Buffer switching off
			ADC_ALT_INPUT_OFF;		// Alternating input off
	ADCON3 = ADC_SAMPLE_TIME_31 &	// 31 clocks for sample
			ADC_CONV_CLK_SYSTEM &	// Use system clock
			ADC_CONV_CLK_32Tcy;		// Wait 32 Tcy
	ADCHS = ADC_CH0_POS_SAMPLEA_AN5;// Sample AN5 only
	ADCSSL = 0X20;					// Scan only AN5
	ADCON1 = ADC_MODULE_ON &		// Turn on ADC
			ADC_IDLE_CONTINUE &		// Don't sample if CPU idle
			ADC_FORMAT_FRACT &		// Fraction data format
			ADC_CLK_AUTO &			// Internal counter starts conversion
			ADC_SAMPLE_INDIVIDUAL &	// Do not sample simultaneously
			ADC_AUTO_SAMPLING_ON &	// Auto sampling
			ADC_SAMP_OFF;			// Hold amplifiers are holding

	// Setup LED port
	LATE = 0x06;					// Initially LEDs 2 and 4 on
	TRISE = 0x0;					// Configure PORTE pins as output

	// Timer used to flash LEDs in interrupt context
	TMR1 = 0;						// clear timer 1
	PR1 = 0x7270;					// interrupt every 250ms
	IFS0bits.T1IF = 0;				// clr interrupt flag
	IEC0bits.T1IE = 1;				// set interrupt enable bit
	T1CON = 0x8030;					// Fosc/4, 1:256 prescale, start TMR1

	// UART setup
	TRISF = 0xC;					// UART on RF2/3
	U1BRG  = BRGVAL;				// Set baud rate generator
	U1MODE = 0x8000; 				// Reset UART to 8-n-1, alt pins, and enable 
	U1STA  = 0x0440; 				// Reset status register and enable TX & RX

	return;
}
