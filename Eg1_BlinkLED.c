/**********************************************************************************
* � 2007 Microchip Technology Inc.
*
* FileName:        Eg1_BlinkLED.c
* Dependencies:    p30f2010.h
* Processor:       dsPIC30F2010
* Compiler:        MPLAB� C30 v3
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip,s 
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP,S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
********************************************************************************************
*
* Description:  In this example program characters pressed on the keyboard are sent to
* the device via usb. The device replicates the character back to the PC enclosed in quotes. 
* Also this program blinks all four LEDs at 250ms by using timer 1. 
*
* McD's modifications:
*   - Instead of just quotes a friendly message is sent
*   - When certain characters are pressed, a long message is sent
*   - LEDs initialized to 5 so XORing produces 0x5-0xa display
*   - UART operations consolidated into separate functions
*
*******************************************************************************************/ 

#include "p30F2010.h"

#define XTFREQ          7372800         		//On-board Crystal frequency
#define PLLMODE         16               		//On-chip PLL setting
#define FCY             XTFREQ*PLLMODE/4        //Instruction Cycle Frequency
#define BAUDRATE         9600       
#define BRGVAL          ((FCY/BAUDRATE)/16)-1 	// Value for the baud rate generator

// Various messages to be sent over USB
const char Message[] = "The character sent was \"";
const char Msg1[] = "\r\nI suppose it shouldn't be a surprise that text of this\r\nlength can be stored in the PIC.  After all, the PIC\r\nmight well have prodigious memory.\r\n";
const char Msg2[] = "\r\nNot only that, there are several messages stored here,\r\nselectable depending on what character happens to be\r\npressed by the unsuspecting user.\r\n";
const char Msg3[] = "\r\nThe main difficulty with this exercise is thinking of things\r\nto say to fill up memory.  I need to think of some\r\nmeaningful application where all this text might be interesting.\r\n";
const char Msg4[] = "\r\nWe the people of the United States, in order to form a\r\nmore perfect union, establish justice, ensure domestic\r\ntranquility, provide for the common defense, support the\r\ngeneral welfare, and obtain the blessings of liberty for\r\nourselves and our posterity do hereby ordain and establish\r\nthis constitution of the United States of America.\r\n";

// Function to get a character from the USB port
int getch( void )
{
	while (_U1RXIF==0);			// Wait and receive one Character
	return U1RXREG;				// Return the character in the UART rcv reg
}

// function to send a character over USB
void putch( char ch )
{
		while(!U1STAbits.TRMT);	// Wait for prev char to complete
		U1TXREG = ch;			// char to UART xmit register
}

// Function to send a message
void send( char *p )
{

	while ( *p )				// until terminating null
		putch( *p++ );			// send the character
}

// Function to echo back the character as part of a message
void sendMessage( char ch )
{
	send( (char *)Message );	// First part of message
	putch(ch);					// Echo the character
	send( "\"\r\n");			// Finish with quote and newline
}

// Mainline starts here
int main(void)
{

	// Port setup
	ADPCFG = 0xFF;				//Make analog pins digital 
	LATE = 0x05;
	TRISE = 0x0;				//Configure LED pins as output

	// Timer used to flash LEDs in interrupt context
	TMR1 = 0;					// clear timer 1
	PR1 = 0x7270;				// interrupt every 250ms
	IFS0bits.T1IF = 0;			// clr interrupt flag
	IEC0bits.T1IE = 1;			// set interrupt enable bit
	T1CON = 0x8030;				// Fosc/4, 1:256 prescale, start TMR1

	// UART setup
	TRISF = 0xC;
	U1BRG  = BRGVAL;
	U1MODE = 0x8000; 			// Reset UART to 8-n-1, alt pins, and enable 
	U1STA  = 0x0440; 			// Reset status register and enable TX & RX

	_U1RXIF=0;					// Clear UART RX Interrupt Flag


	// "1" Isn't going to go false anytime soon
	while(1)
	{
		int a;	

		a = getch();			// Wait and receive one Character

		switch ( a )
		{
			case 'A':
				send((char *)Msg1);
				break;
			case 'B':
				send((char *)Msg2);
				break;
			case 'C':
				send((char *)Msg3);
				break;
			case 'D':
				send((char *)Msg4);
				break;
			default:
				sendMessage(a);
		}
	
		_U1RXIF=0;				// Clear UART RX Interrupt Flag
			
	}
return 0;
}


void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;		// clear interrupt flag
	
	LATE ^= 0xF;			//Toggle LED's
}


