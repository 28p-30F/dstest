; $Id: Tst30-PortIni.s,v 1.2 2006-04-20 14:54:22-04 jjmcd Exp jjmcd $
;		Port initialization for Tst30
       .equ __30F2010, 1
        .include "p30f2010.inc"

		.global		port_init
		.extern		timelord

;..............................................................................
;Subroutine: Initialization of PORTC
;..............................................................................
port_init:
		MOV			#0xa000,W2		; PORTC only has <13:15>
		MOV			W2,TRISC
		RETURN

.end
