; #Id$
;	Dummy ISR for traps to make debugging easier
;
;	Basically, all this module does is provide a place to send
;	the processor in the event of a trap. Although these routines
;	make no attempt to deal with the trap, any of these exceptions
;	will now cause the processor to hang at a recognizable
;	location, rather than reset.  Probably no preferable for a
;	release version, but better for debugging.

			.global		__OscillatorFail,__AddressError,__StackError,__MathError

.text
__OscillatorFail:
			GOTO		.

__AddressError:
			GOTO		.

__StackError:
			GOTO		.

__MathError:
			GOTO		.

			.end
