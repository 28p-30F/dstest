; $Id: Tst30-3.s,v 1.3 2006-04-20 17:43:36-04 jjmcd Exp jjmcd $
;		DSPic flash a LED using timer interrupt
;
			.equ		__30F2010, 1
			.include	"p30f2010.inc"

;..............................................................................
;Configuration bits:
;..............................................................................

			config		__FOSC, CSW_FSCM_OFF & XT_PLL8    ;Turn off clock 
												;switching and fail-safe clock
												;monitoring and use the External
												;Clock as the system clock

			config		__FWDT, WDT_OFF			;Turn off Watchdog Timer

			config		__FBORPOR, PBOR_OFF & BORV_27 & PWRT_16 & MCLR_EN
												;Set Brown-out Reset voltage and
												;and set Power-up Timer to 16msecs
                                            
			config		__FGS, CODE_PROT_OFF	;Set Code Protection Off for the 
												;General Segment

;..............................................................................
;Global Declarations:
;..............................................................................

			.global 	__reset					;The label for the first line of code. 

			.section	variables, bss, near
value1	:	.space		2						; Count off time
			.section	.ybss, bss, ymemory
y_input:	.space		128						; 2*64


.text											;Start of Code section
__reset:
			MOV			#__SP_init, W15			;Initalize the Stack Pointer
			MOV			#__SPLIM_init, W0		;Initialize the Stack Pointer Limit Register
			MOV			W0, SPLIM
			NOP									;Add NOP to follow SPLIM initialization

			MOV			#0,W2					;Clear out test value
			MOV			W2,value1

loop:
			INC			value1
			GOTO		loop
        
.end
