;		DSPic flash a LED using timer interrupt
       .equ __30F2010, 1
        .include "p30f2010.inc"

;..............................................................................
;Configuration bits:
;..............................................................................

        config __FOSC, CSW_FSCM_OFF & XT_PLL16    ;Turn off clock switching and
                                            ;fail-safe clock monitoring and
                                            ;use the External Clock as the
                                            ;system clock

        config __FWDT, WDT_OFF              ;Turn off Watchdog Timer

        config __FBORPOR, PBOR_OFF & BORV_27 & PWRT_16 & MCLR_EN
                                            ;Set Brown-out Reset voltage and
                                            ;and set Power-up Timer to 16msecs
                                            
        config __FGS, CODE_PROT_OFF         ;Set Code Protection Off for the 
                                            ;General Segment

;..............................................................................
;Global Declarations:
;..............................................................................

        .global __reset          ;The label for the first line of code. 

        .global __T1Interrupt    ;Declare Timer 1 ISR name global

;..............................................................................
;Uninitialized variables in Near data memory (Lower 8Kb of RAM)
;..............................................................................

          .section .nbss, bss, near
timelord:	.space		2

;..............................................................................
;Code Section in Program Memory
;..............................................................................

.text								;Start of Code section
__reset:
        MOV #__SP_init, W15			;Initalize the Stack Pointer
        MOV #__SPLIM_init, W0		;Initialize the Stack Pointer Limit Register
        MOV W0, SPLIM
        NOP							;Add NOP to follow SPLIM initialization
        
		CALL		port_init		; Initialize PORTC<14> as output

		CALL		timer_init		; Initialize TIMER1

loop:
		CP0			timelord		; Time expired?
		BRA			Z,loop			; No, check again
	; Toggle the LED
		MOV			#0x4000,W0		; Toggle PORTC<14> by XORing
		XOR			PORTC			; with 0x4000
		CLR			timelord
		BRA			loop			; Do it again

;..............................................................................
;Subroutine: Initialization of TMR1
;..............................................................................
timer_init:
		MOV			#0x0020,W0		; Turn off timer, set prescale
		MOV			W0,T1CON		; to 64:1
		CLR			TMR1			; Clear timer register
		MOV			#0xffff,W0		; Initialize period register
		MOV			W0,PR1			; to maximum

		BSET		IPC0,#T1IP0		; Setup TMR1 interrupt for
		BCLR		IPC0,#T1IP1		; desired priority level
		BCLR		IPC0,#T1IP2		;

		BCLR		IFS0,#T1IF		; Clear the interrupted flag
		BSET		IEC0,#T1IE		; Enable timer interrup

		CLR			timelord		; Clear the time counter

		BSET		T1CON,#TON		; Turn on the timer
		RETURN

;..............................................................................
;Subroutine: Initialization of PORTC
;..............................................................................
port_init:
		MOV			#0xa000,W2		; PORTC only has <13:15>
		MOV			W2,TRISC
		RETURN

;..............................................................................
;Timer 1 Interrupt Service Routine
;Example context save/restore in the ISR performed using PUSH.D/POP.D
;instruction. The instruction pushes two words W4 and W5 on to the stack on
;entry into ISR and pops the two words back into W4 and W5 on exit from the ISR
;..............................................................................

__T1Interrupt:
		PUSH.D	W4					;Save context using double-word PUSH

		INC		timelord			; Increment the time counter

		BCLR	IFS0, #T1IF			;Clear the Timer1 Interrupt flag Status
									;bit.
		POP.D	W4					;Retrieve context POP-ping from Stack
		RETFIE						;Return from Interrupt Service routine

.end								;End of program code in this file

