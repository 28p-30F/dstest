; $Id:$
;		DSPic grab RA0 into circular buffer
;
            .equ        __30F2010, 1
            .include    "p30f2010.inc"
            .list       b=4

;..............................................................................
; Configuration bits:
;..............................................................................

            config      __FOSC, CSW_FSCM_OFF & XT_PLL8    ; Turn off clock 
                                                ; switching and fail-safe clock
                                                ; monitoring and use the External
                                                ; Clock as the system clock

			config		__FWDT, WDT_OFF			; Turn off Watchdog Timer

			config		__FBORPOR, PBOR_OFF & BORV_27 & PWRT_16 & MCLR_EN
												; Set Brown-out Reset voltage and
												; and set Power-up Timer to 16msecs
                                            
			config		__FGS, CODE_PROT_OFF	; Set Code Protection Off for the 
												; General Segment

;..............................................................................
; Global Declarations:
;..............................................................................

			.global 	__reset					; The label for the first line of code. 

			.section	variables, bss, near
value1:		.space		2						; Test variable
pointer:	.space		2						; Address in buffer

			.section	.ybss, bss, ymemory
y_input:	.space		128						; 2*64


.text											; Start of Code section
__reset:
			mov			#__SP_init, W15			; Initalize the Stack Pointer
			mov			#__SPLIM_init, W0		; Initialize the Stack Pointer Limit Register
			mov			W0, SPLIM
			nop									; Add NOP to follow SPLIM initialization

			call		analog_init				; Initialize A/D
			mov			#0,W2					; Clear out test value
			mov			W2,value1
loopR:
			mov			#y_input,W2				; Initialize result
			mov			W2,pointer				; pointer

loop:
			inc			value1					; Left over from earlier test
adloop:
			btss		ADCON1,#DONE			; Loop until A/D
			goto		adloop					; conversion is done
			mov			ADCBUF0,W2				; Pick up value from A/D
			mov			pointer,W4				; and location to store
			mov			W2,[W4]					; Now store the value
			inc2		pointer					; Point to next location
			mov			#y_input+128,W0			; See if it wrapped
			subbr		pointer,WREG			;
			bra			NN,loop					; Didn't wrap, do next
			bra			loopR					; Reset to begin buffer
        
.end
