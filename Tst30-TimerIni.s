; $Id: Tst30-TimerIni.s,v 1.2 2006-04-20 14:55:06-04 jjmcd Exp jjmcd $
;		Timer initialization for Tst30
       .equ __30F2010, 1
        .include "p30f2010.inc"

		.global		timer_init
		.extern		timelord

;..............................................................................
;Subroutine: Initialization of TMR1
;..............................................................................
timer_init:
		MOV			#0x0020,W0		; Turn off timer, set prescale
		MOV			W0,T1CON		; to 64:1
		CLR			TMR1			; Clear timer register
		MOV			#0x0010,W0		; Initialize period register
		MOV			W0,PR1			; 

		BSET		IPC0,#T1IP0		; Setup TMR1 interrupt for
		BCLR		IPC0,#T1IP1		; desired priority level
		BCLR		IPC0,#T1IP2		;

		BCLR		IFS0,#T1IF		; Clear the interrupted flag
		BSET		IEC0,#T1IE		; Enable timer interrup

		CLR			timelord		; Clear the time counter

		BSET		T1CON,#TON		; Turn on the timer
		RETURN


.end
