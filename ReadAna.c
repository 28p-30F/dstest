/*! \file ReadAna.c

 \brief Send analog to PC

 In this example program the analog value from the pot is read and
 sent to the PC.

 Also this program blinks all four LEDs at 250ms by using timer 1.

\li Processor : PIC30F4012
\li Compiler  : C30 v3 (required)
*/


// Basic 30F4012 definitions
#include <p30F4012.h>
// A/D converter bit definitions
#include "peripheral_30F_24H_33F/adc10.h"
// C I/O library definitions
#include <stdio.h>

//! Counter incremented in timer 1 context, used to limit data sent to PC
int Ticker;

#define CLEAR "\033[0;40;36m\033[H\033[2J"
#define GRAY  "\033[0;40;37m"
#define RED   "\033[0;40;31;1m"
#define GREEN "\033[0;40;32m"

void myIni( void );

//! main - Read the analog
/*!  main() first initializes the timer and ports, then goes into
     an infinite loop. It checks the variable Ticker, and whenever
     it reaches 8, it reads the analog channel 0 and sends the result
     to the PC over USB.  Ticker is incremented in the interrupt context.

Pseudocode:
\code
  myIni()
  Ticker = 0
  Print startup banner
  do forever
    if Ticker >= 8
      Ticker = 0
      Get ADC result
      Convert to fraction and voltage
      Print results on stdout
    end if
  end do
\endcode
*/
int main(void)
{
	//! Local variables
	//! fFrac - Fractional (0.0-1.0) value
	double fFrac;
	//! fVolt - Reading in volts
	double fVolt;
	//! nValue - Raw integer from A/D
	unsigned int nValue;
	//! nPrev - Previous value from A/D
	unsigned int nPrev;
	//! nCount - number of values displayed so far
	int nCount;

	myIni();						// Initialize peripherals

	Ticker = 0;						// Initialize print counter
	nCount = 0;
	nPrev  = 65535;

	// Mark program restart on HyperTerm window
	printf("%s\r\n=============================\r\n\n",CLEAR);

	// "1" Isn't going to go false anytime soon
	while(1)
	{
		// Every 2 seconds ( 8 * 250 ms )
		if ( Ticker >= 8 )
		{
			Ticker = 0;				// Reset Ticker
			nValue = ADCBUF2;		// Get value from A/D
			if ( nValue != nPrev )
			{
				// Convert to scaled fraction
				fFrac = (double) nValue / 65536.0;
				// And also to Voltage
				fVolt = 5.00 * fFrac;
				// Display the result on Hyperterm
				printf("%s%5d%s Integer 0x%04x frac %6.4f %sresult %6.3fV.\r\n",
					GREEN,++nCount,GRAY,nValue,fFrac,RED,fVolt);
				nPrev = nValue;
			}
		}
	}

	return 1;
}


//! _T1Interrupt - Increment Ticker
/*! Timer 1 interrupt every 250 ms.  Toggle LEDs and 
    increment Ticker so A/D display only each 2 sec.

Pseudocode:
\code
Clear interrupt flag
Invert LEDs
Ticker = Ticker + 1
\endcode
*/
void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;		// clear interrupt flag
	
	LATE ^= 0xF;			// Toggle LED's
	Ticker++;				// Bump counter
}
