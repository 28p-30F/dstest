	.file "/home/jjmcd/usr/U067844/Projects/tstC30/Test.c"
; GNU C version 3.3 (dsPIC30, Microchip 1.32, modified for Debian GNU/Linux) Build date: Dec  9 2005 (pic30-elf)
;	compiled by GNU C version 4.0.2 20051125 (Red Hat 4.0.2-8).
; GGC heuristics: --param ggc-min-expand=98 --param ggc-min-heapsize=128328
; options passed:  -D__GNUC__=3 -D__GNUC_MINOR__=3 -D__GNUC_PATCHLEVEL__=0
; -DC30 -DC30ELF -DdsPIC30 -DdsPIC30ELF -D__C30__ -D__C30ELF__
; -D__dsPIC30__ -D__dsPIC30ELF__ -D__C30 -D__C30ELF -D__dsPIC30
; -D__dsPIC30ELF -auxbase -fverbose-asm
; options enabled:  -fpeephole -ffunction-cse -fkeep-static-consts
; -freg-struct-return -fgcse-lm -fgcse-sm -fsched-interblock -fsched-spec
; -fbranch-count-reg -fcommon -fverbose-asm -fgnu-linker -fargument-alias
; -fzero-initialized-in-bss -fident -fmath-errno -ftrapping-math -msmart-io

	.align	2
	.global	_main	; export
;	.type	_main,@function
_main:
	.set ___PA___,1
	lnk	#2
	nop	
.L2:
	clr	w0
	mov	w0,[w14]	;  i
.L5:
	mov	#9,w0
	subr	w0,[w14],[w15]	;  i
	.set ___BP___,0
	bra	gt,.L2
	mov	[w14],w0	;  i
	rcall	_wait
	inc	[w14],[w14]	;  i,  i
	bra	.L5
	.align	2
	.global	_wait	; export
;	.type	_wait,@function
_wait:
	.set ___PA___,1
	lnk	#6
	mov	w0,[w14]	;  n
	clr	w0
	mov	w0,[w14+2]	;  i
.L10:
	mov	[w14+2],w0	;  i
	sub	w0,[w14],[w15]	;  n
	.set ___BP___,0
	bra	ge,.L9
	clr	w0
	mov	w0,[w14+4]	;  j
.L14:
	mov	[w14+4],w1	;  j
	mov	#16382,w0
	sub	w1,w0,[w15]
	.set ___BP___,0
	bra	gt,.L12
	mov	[w14+4],w0	;  j
	inc	w0,w0
	mov	w0,[w14+4]	;  j
	bra	.L14
.L12:
	mov	[w14+2],w0	;  i
	inc	w0,w0
	mov	w0,[w14+2]	;  i
	bra	.L10
.L9:
	ulnk	
	return	
	.set ___PA___,0

	.end
