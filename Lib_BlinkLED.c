/**********************************************************************************
* � 2007 Microchip Technology Inc.
*
* FileName:        Eg1_BlinkLED.c
* Dependencies:    p30f2010.h
* Processor:       dsPIC30F2010
* Compiler:        MPLAB� C30 v3
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip,s 
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP,S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
********************************************************************************************
*
* Description:  In this example program characters pressed on the keyboard are sent to
* the device via usb. The device replicates the character back to the PC enclosed in quotes. 
* Also this program blinks all four LEDs at 250ms by using timer 1. 
*
* McD's modifications:
*   - Instead of just quotes a friendly message is sent
*   - When certain characters are pressed, a long message is sent
*   - LEDs initialized to 5 so XORing produces 0x5-0xa display
*   - UART operations consolidated into separate functions
*
*******************************************************************************************/ 

#include <p30F4012.h>
#include <stdio.h>

#define XTFREQ          7372800         		//On-board Crystal frequency
#define PLLMODE         16               		//On-chip PLL setting
#define FCY             XTFREQ*PLLMODE/4        //Instruction Cycle Frequency
#define BAUDRATE         9600       
#define BRGVAL          ((FCY/BAUDRATE)/16)-1 	// Value for the baud rate generator

// Various messages to be sent over USB
const char Message[] = "The character sent was \"";
const char Msg1[] = "\r\nI suppose it shouldn't be a surprise that text of this\r\nlength can be stored in the PIC.  After all, the PIC\r\nmight well have prodigious memory.\r\n";
const char Msg2[] = "\r\nNot only that, there are several messages stored here,\r\nselectable depending on what character happens to be\r\npressed by the unsuspecting user.\r\n";
const char Msg3[] = "\r\nThe main difficulty with this exercise is thinking of things\r\nto say to fill up memory.  I need to think of some\r\nmeaningful application where all this text might be interesting.\r\n";
const char Msg4[] = "\r\nWe the people of the United States, in order to form a\r\nmore perfect union, establish justice, ensure domestic\r\ntranquility, provide for the common defense, promote the\r\ngeneral welfare, and obtain the blessings of liberty for\r\nourselves and our posterity do ordain and establish\r\nthis constitution of the United States of America.\r\n";
const char Msg5[] = "\r\nWhen in the Course of human events it becomes\r\nnecessary for one people to dissolve the\r\npolitical bands which have connected them with\r\nanother and to assume among the powers of the\r\nearth, the separate and equal station to which\r\nthe Laws of Nature and of Nature's God entitle\r\nthem, a decent respect to the opinions of mankind\r\nrequires that they should declare the causes\r\nwhich impel them to the separation.\r\n";
const char Msg6[] = "\r\nWe hold these truths to be self-evident, that all\r\nmen are created equal, that they are endowed by their\r\nCreator with certain unalienable Rights, that among these\r\nare Life, Liberty and the pursuit of Happiness. - That to\r\nsecure these rights, Governments are instituted among Men,\r\nderiving their just powers from the consent of the governed,\r\n- That whenever any Form of Government becomes destructive of\r\nthese ends, it is the Right of the People to alter or to\r\nabolish it, and to institute new Government, laying its\r\nfoundation on such principles and organizing its powers in such\r\nform, as to them shall seem most likely to effect their Safety\r\nand Happiness.\r\n";
const char Msg7[] = "\r\nPrudence, indeed, will dictate that Governments\r\nlong established should not be changed for light\r\nand transient causes; and accordingly all experience\r\nhath shewn that mankind are more disposed to suffer,\r\nwhile evils are sufferable than to right themselves\r\nby abolishing the forms to which they are accustomed.\r\nBut when a long train of abuses and usurpations,\r\npursuing invariably the same Object evinces a design\r\nto reduce them under absolute Despotism, it is their\r\nright, it is their duty, to throw off such Government,\r\nand to provide new Guards for their future security.\r\n- Such has been the patient sufferance of these\r\nColonies; and such is now the necessity which constrains\r\nthem to alter their former Systems of Government. The\r\nhistory of the present King of Great Britain is a history\r\nof repeated injuries and usurpations, all having in\r\ndirect object the establishment of an absolute Tyranny\r\nover these States. To prove this, let Facts be submitted\r\nto a candid world.\r\n";

// Function to get a character from the USB port
//
// Haven't quite sorted why the getchar() library function
// doesn't seem to behave so well.
int getch( void )
{
	while (_U1RXIF==0);			// Wait and receive one Character
	return U1RXREG;				// Return the character in the UART rcv reg
}


// Mainline starts here
int main(void)
{
	double b;

	// Port setup
	ADPCFG = 0xFF;				// Make analog pins digital 
	LATE = 0x05;				// Initially LEDs 2 and 4 on
	TRISE = 0x0;				// Configure LED pins as output

	// Timer used to flash LEDs in interrupt context
	TMR1 = 0;					// clear timer 1
	PR1 = 0x7270;				// interrupt every 250ms
	IFS0bits.T1IF = 0;			// clr interrupt flag
	IEC0bits.T1IE = 1;			// set interrupt enable bit
	T1CON = 0x8030;				// Fosc/4, 1:256 prescale, start TMR1

	// UART setup
	TRISF = 0xC;
	U1BRG  = BRGVAL;
	U1MODE = 0x8000; 			// Reset UART to 8-n-1, alt pins, and enable 
	U1STA  = 0x0440; 			// Reset status register and enable TX & RX

	_U1RXIF=0;					// Clear UART RX Interrupt Flag


	b = 1.01;

	// "1" Isn't going to go false anytime soon
	while(1)
	{
		int a,ch;

		a = getch();

		switch ( a )
		{
			case 'A':
				printf(Msg1);
				break;
			case 'B':
				printf(Msg2);
				break;
			case 'C':
				printf(Msg3);
				break;
			case 'D':
				printf(Msg4);
				break;
			case 'E':
				printf(Msg5);
				break;
			case 'F':
				printf(Msg6);
				break;
			case 'G':
				printf(Msg7);
				break;
			default:
				if ( a < ' ' )
					ch = 0xb2;
				else
					ch = a;
				printf("%s%c\" (0x%x)\r\n",Message, ch, a);
		}

		b = b * 1.782;
		printf("\tb is %g\r\n\n",b);
	
		_U1RXIF=0;				// Clear UART RX Interrupt Flag
			
	}
return 0;
}


void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;		// clear interrupt flag
	
	LATE ^= 0xF;			//Toggle LED's
}


