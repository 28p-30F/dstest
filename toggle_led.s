;		DSPic flash a LED
;       .equ __30F2010, 1
        .include "p30f2010.inc"

        .global toggle_led
.text
toggle_led:
		mov			#0x000f,W0		; Toggle PORTC<14> by XORing
		xor			LATE			; with 0x4000
		return
.end

