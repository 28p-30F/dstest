#include <p30f4012.h>

// RD0 = LED0, RD1 = LED3
int VD[8] = { 1, 1, 1, 1, 1, 1, 1, 1 };
// RF2 = LED1, RF3 = LED2
int VF[8] = { 0, 0, 0x0, 0x0, 0, 0, 0x0, 0x0 };
// RE5 = g, RE4 = h(.), RE8 = f
int VE[8] = { 0x00f, 0x01f, 0x02f, 0x03f, 0x13f, 0x12f, 0x16f, 0x10f  };
// RB0 = a, RB1 = b, RB3 = c, RB3 = d, RB4 = e
int VB[8] = { 0x00, 0x00, 0x10, 0x00, 0x10, 0x00, 0x00, 0x00 };

int main()
{
	int i,j,k;
	int tempE;

	TRISD=0xfffc;
	//TRISB=0x0f00;
	TRISF=0xfff3;
	TRISE=0xfec0;
	TRISB=0xffe0;

	while ( 1 )
		{
			for ( i=0; i<8; i++ )
				{
					tempE = VE[i];
					if ( PORTDbits.RD1 )
						tempE ^= 0x80;
					LATD=VD[i];
					LATE=tempE;
					LATF=VF[i];
					LATB= VB[i];
					for ( j=0; j<5000; j++ )
						for ( k=0; k<100; k++ )
							;
				}
		}

	return 0;	
}
