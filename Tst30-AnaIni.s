; $Id: Tst30-AnaIni.s,v 1.2 2006-04-20 14:53:38-04 jjmcd Exp jjmcd $
;	Analog initialization for Tst30
       .equ __30F2010, 1
        .include "p30f2010.inc"

		.global		analog_init

;..............................................................................
;Subroutine: Initialization of AN0
;..............................................................................
analog_init:
	; Select port pins as analog inputs (ADPCFG<15:0>=0)
	; Each bit is set to 0 to set the corresponding A/D
	; port to analog
		CLR			ADPCFG
	; Select voltage reference source (ADCON2<15:13>=0)
	; Bits 15-13 determine the reference source, 0=AVdd/AVss
	; bit 10 set to 0 to not scan inputs
	; bits 9-8 set to 00 to convert only CH0
	; bit 7 set by processor (buffer fill status)
	; bits 5-2 sample/convert sequences, 000=interrupt
	;          at end of conversion for each sample
	; bit 1 - buffer mode selection 0=one 16-bit buffer
	; bit 0 - alternate input sample mode, 0=always use MUXA
		CLR			ADCON2
	; Select analog conversion clock (ADCON3<5:0>=1, ADCON3<ADRC(7)>=1)
	; ADCON3<12-8>=2 .. 2xTad
	; bits 12-8 = auto sample time 00010=2 Tad
	; bit 7 = A/D clock source, 1=RC, 0=Tosc
	; bit 5-0 clock select bits, 3f=32 Tcy
		MOV			#0x02bf,W0
		MOV			W0,ADCON3
	; Determine S/H channels (ADCHS<15:0>=0)
	; High 8 bits channel B, low channel A
	; Determines positive and negative input for each channel
	; 0=Negative Vref- all, CH0 positive=AN0
		CLR			ADCHS
	; Determine how sampling will occur (ADCON1<ASAM(2)>=1)
	; <FORM(9:8)>=10 fractional <SSRC(7:5)>=111 autoconvert
	; bit 15 - ADON 1=turn on converter
	; bit 13 - 1=discontinue operation in IDLE
	; bit 9-8 - Format, 10=fractional
	; bit 7-5 - conversion trigger 111=autoconvert
	; bit 3 - SIMSAM, simultaneous sample
	; bit 2 - ASAM - 1=sample immediately after previous
	; bit 1 - SAMP - Sampling in progress
	; bit 0 - DONE - conversion complete
		MOV			#0x02e4,W0
		MOV			W0,ADCON1
	; Turn on A/D module (ADCON1<ADON>=1)
		BSET		ADCON1,#ADON
		RETURN
.end
