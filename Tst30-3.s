; $Id: Tst30-3.s,v 1.3 2006-04-20 17:43:36-04 jjmcd Exp jjmcd $
;		DSPic flash a LED using timer interrupt
;
       .equ __30F2010, 1
        .include "p30f2010.inc"

;..............................................................................
;Configuration bits:
;..............................................................................

        config __FOSC, CSW_FSCM_OFF & XT_PLL8    ;Turn off clock switching and
										;fail-safe clock monitoring and
										;use the External Clock as the
										;system clock

        config __FWDT, WDT_OFF			;Turn off Watchdog Timer

        config __FBORPOR, PBOR_OFF & BORV_27 & PWRT_16 & MCLR_EN
										;Set Brown-out Reset voltage and
										;and set Power-up Timer to 16msecs
                                            
        config __FGS, CODE_PROT_OFF		;Set Code Protection Off for the 
										;General Segment

;..............................................................................
;Global Declarations:
;..............................................................................

        .global 	__reset				;The label for the first line of code. 

		.global		timelord,pointer,y_input

		.extern		timer_init,port_init,analog_init,data_init

;..............................................................................
;Uninitialized variables in Near data memory (Lower 8Kb of RAM)
;..............................................................................

          .section variables, bss, near
timelord:	.space		2				; Count off time
adreslt:	.space		2				; A/D result
pointer:	.space		2				; Pointer to saved results
maxval:		.space		2
midval:		.space		2
prevval:	.space		2
val3:		.space		2
val1:		.space		2

;..............................................................................
;Uninitialized variables in Y-space in data memory
;..............................................................................

          .section .ybss, bss, ymemory
y_input:  .space 256		; 2*128

;..............................................................................
;Code Section in Program Memory
;..............................................................................

.text								;Start of Code section
__reset:
        MOV #__SP_init, W15			;Initalize the Stack Pointer
        MOV #__SPLIM_init, W0		;Initialize the Stack Pointer Limit Register
        MOV W0, SPLIM
        NOP							;Add NOP to follow SPLIM initialization
        
		CALL		data_init		; Clear data space
		CALL		port_init		; Initialize PORTC<14> as output
		CALL		analog_init		; Initialize AN0
		CALL		timer_init		; Initialize TIMER1

		MOV			#0xe000,W2
		MOV			W2,maxval
		MOV			#0x8000,W2
		MOV			W2,prevval

loop:
		MOV			#0x0002,W0		; Time has run out when
		CP			timelord		; timelord reaches 0x0002
		BRA			NZ,loop			; No, check again
	; Do the A/D conversion
adloop:
		BTSS		ADCON1,#DONE	; Loop until A/D
		GOTO		adloop			; conversion is done
		MOV.W		ADCBUF0,W2		; Grab the result
		MOV			W2,adreslt		; and store it

;	Store A/D result into table
		INC2		pointer			; Point to next table loc
		BTSS		pointer,#9		; Kind of strange wrap
		GOTO		store_result	; check, there is supposed
		MOV			#y_input,W3		; to be some automatic way
		MOV			W3,pointer
store_result:
		MOV			pointer,W3		; Pick up result pointer
		MOV			W2,[W3]			; and store result in table

;	Make LED dim for part of travel
test_range:
		MOV			midval,W0		; Check whether midrange
		CP			adreslt			; If greater, go to
		BRA			NN,toggle		; toggle LED
		BCLR		PORTC,#14		; else turn on LED and
		GOTO		retime			; reset timelord

	; Toggle the LED
toggle:
		MOV			#0x4000,W0		; Toggle PORTC<14> by XORing
		MOV			W0,PORTC		; with 0x4000
retime:
		CLR			timelord
		CALL		filter
		CALL		calc_middle
		BRA			loop			; Do it again

calc_middle:
		CLR			maxval
		MOV			#y_input,W3

		DO			#0x7f,calc_doend
		MOV			maxval,W0
		CP			W0,[W3]
		BRA			NN,calc_mid2
		MOV			[W3],W0
		MOV			W0,maxval
calc_mid2:
		INC2		WREG3
calc_doend:
		MOV			maxval,W0
		ASR			WREG0
		MOV			W0,midval
		RETURN

filter:
		LSR			prevval
		LSR			W0,W0
		MOV			W0,val1
		LSR			prevval
		ADD			val1
		MOV			W0,val3			; val3 now contains 3/4 prev
		RETURN		

.end								;End of program code in this file

