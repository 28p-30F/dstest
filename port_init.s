;		DSPic flash a LED
;       .equ __30F2010, 1
        .include "p30f2010.inc"

        .global port_init

.text								;Start of Code section
;..............................................................................
;Subroutine: Initialization of PORTC
;..............................................................................
port_init:
		MOV			#0x0000,W2		; 
		MOV			W2,TRISE
		RETURN

.end
