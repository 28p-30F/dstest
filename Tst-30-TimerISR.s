; $Id: Tst-30-TimerISR.s,v 1.2 2006-04-20 14:55:36-04 jjmcd Exp jjmcd $
;		Timer ISR for Tst30
       .equ __30F2010, 1
        .include "p30f2010.inc"

        .global __T1Interrupt    ;Declare Timer 1 ISR name global

		.extern		timelord

;..............................................................................
;Timer 1 Interrupt Service Routine
;Example context save/restore in the ISR performed using PUSH.D/POP.D
;instruction. The instruction pushes two words W4 and W5 on to the stack on
;entry into ISR and pops the two words back into W4 and W5 on exit from the ISR
;..............................................................................

__T1Interrupt:
		PUSH.D	W4					;Save context using double-word PUSH

		INC		timelord			; Increment the time counter

		BCLR	IFS0, #T1IF			;Clear the Timer1 Interrupt flag Status
									;bit.
		POP.D	W4					;Retrieve context POP-ping from Stack
		RETFIE						;Return from Interrupt Service routine

.end
