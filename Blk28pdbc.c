#include <p30f2010.h>

// Function to wait a long time
void snore( void );

int main()
{
	int count;	// Variable to hold count to send to LEDs

	TRISE = 0;	// All pins output
	count = 0;	// Initialize count

	// Loop forever
	while ( 1 )
	{
		// Wait a while
		snore();
		// Send LSBs of count to LEDs, need to invert because
		// LEDs light when portbit is false
		LATE = ( count ^ 0x0f ) & 0x0f;
		// If button pressed
		if ( !PORTDbits.RD1 )
			// Toggle all LEDs
			count ^= 0x0f;
		else
			// otherwise increment count
			count++;
	}
	
}
